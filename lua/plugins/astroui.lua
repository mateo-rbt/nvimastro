-- if true then return {} end -- WARN: REMOVE THIS LINE TO ACTIVATE THIS FILE

-- AstroUI provides the basis for configuring the AstroNvim User Interface
-- Configuration documentation can be found with `:h astroui`
-- NOTE: We highly recommend setting up the Lua Language Server (`:LspInstall lua_ls`)
--       as this provides autocomplete and documentation while editing

local function getTheme()
  local light_theme = "astrolight"
  local dark_theme = "obscure"

  local home = os.getenv "HOME"
  local desktop_theme_file = io.open(home .. "/.desktop_theme", "rb")

  if not desktop_theme_file then
    vim.notify("cannot find ~/.desktop_theme file, using dark theme", vim.log.levels.INFO, {
      title = "AstroUI",
    })
    return dark_theme
  end

  local desktop_theme = desktop_theme_file:read "*l"
  desktop_theme_file:close()

  if desktop_theme == nil or desktop_theme == "" then return dark_theme end

  return (desktop_theme == "light") and light_theme or dark_theme
end

---@type LazySpec
return {
  "AstroNvim/astroui",

  ---@type AstroUIOpts
  opts = {
    -- change colorscheme
    -- colorscheme = "astrolight",
    colorscheme = getTheme(),
    -- AstroUI allows you to easily modify highlight groups easily for any and all colorschemes
    highlights = {
      init = { -- this table overrides highlights in all themes
        -- Normal = { bg = "#000000" },
      },
      astrotheme = { -- a table of overrides/changes when applying the astrotheme theme
        -- Normal = { bg = "#000000" },
      },
    },
    -- Icons can be configured throughout the interface
    icons = {
      -- configure the loading of the lsp in the status line
      LSPLoading1 = "⠋",
      LSPLoading2 = "⠙",
      LSPLoading3 = "⠹",
      LSPLoading4 = "⠸",
      LSPLoading5 = "⠼",
      LSPLoading6 = "⠴",
      LSPLoading7 = "⠦",
      LSPLoading8 = "⠧",
      LSPLoading9 = "⠇",
      LSPLoading10 = "⠏",
    },
  },
}
