---@type LazySpec
return {
  { "rose-pine/neovim", name = "rose-pine" },
  { "rebelot/kanagawa.nvim" },
  { "catppuccin/nvim", name = "catppuccin", priority = 1000 },
  { "Mofiqul/vscode.nvim", priority = 1000 },
  { "killitar/obscure.nvim" },
}
