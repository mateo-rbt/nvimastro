---@type LazySpec
return {
  {
    "hrsh7th/nvim-cmp",
    dependencies = {
      "onsails/lspkind-nvim",
    },
    -- Not all LSP servers add brackets when completing a function.
    -- To better deal with this, LazyVim adds a custom option to cmp,
    -- that you can configure. For example:
    --
    -- ```lua
    -- opts = {
    --   auto_brackets = { "python" }
    -- }
    -- ```
    opts = function(_, opts)
      local cmp = require "cmp"
      local lspkind = require "lspkind"
      local compare = cmp.config.compare

      opts.sorting = {
        priority_weight = 2,
        comparators = {
          compare.offset,
          compare.exact,
          compare.score,
          compare.kind,
          compare.sort_text,
          compare.length,
          compare.order,
        },
      }
      table.insert(opts.sorting.comparators, 1, function(entry1, entry2)
        local kind1 = entry1:get_kind()
        local kind2 = entry2:get_kind()
        if kind1 ~= kind2 then
          if kind1 == cmp.lsp.CompletionItemKind.Field then return true end
          if kind2 == cmp.lsp.CompletionItemKind.Field then return false end
        end
      end)

      opts.formatting = {
        format = lspkind.cmp_format {
          mode = "symbol_text", -- Show symbol and text
          maxwidth = 30, -- Limit the width of the popup
          ellipsis_char = "...", -- Show ellipsis if the text is too long
          before = function(entry, vim_item)
            -- vim_item.menu = ({
            --   buffer = "[Buffer]",
            --   nvim_lsp = "[LSP]",
            --   path = "[Path]",
            --   -- Add more sources as needed
            -- })[entry.source.name]

            -- vim_item.menu = entry.completion_item.detail
            vim_item.kind = lspkind.presets.default[vim_item.kind] .. " " .. vim_item.kind

            return vim_item
          end,
        },
      }

      return opts
    end,
  },
}
